/**
* A Shitty Selfbot.
* Author: Keswiik
*/
require('dotenv').config();

const SelfbotClient = require('./classes/SelfbotClient');
const Logger = require('./classes/Logger.js');

let logger = new Logger();
const bot = new SelfbotClient();

bot.login(process.env.TOKEN);

/**
* Catches unhandled promise rejections to prevent the bot from crashing.
*/
process.on('unhandledRejection', err => {
    logger.log('error', err.message);
    logger.log('error', err.stack);
});
