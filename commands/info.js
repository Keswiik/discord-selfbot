const Discord = require('discord.js');
const parseConfig = require('parse-git-config');
const Command = require('../classes/Command.js');

/**
 * A command for showing information about the bot
 * @extends Command
 */
class InfoCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'information about the bot',
            usage: [
                'info'
            ]
        });
        this.gitConfig = parseConfig.sync({cwd: process.cwd(), path: '.git/config'});
    }

    async run(message) {
        let time = this.getTimeString(process.uptime());
        let embed = new Discord.RichEmbed()
            .setAuthor(`${this.bot.user.username}'s Selfbot`)
            .setColor(0x00FFFF)
            .addField('Repository', this.gitConfig['remote "origin"'].url)
            .addField('Heartbeat Ping', Math.floor(this.bot.ping) + 'ms', true)
            .addField('Uptime', time, true)
            .addField('Discord.js Version', `\`v${Discord.version}\``, true)
            .addField('Node.js Version', process.version, true)
            .addBlankField(true);

        message.edit({ embed });
    }

    /**
     * Converts milliseconds into d-h-m-s format
     * @param {number} timeInMs - the time in milliseconds
     * @return {string} the formatted time
     */
    getTimeString(timeInMs) {
        let time = '';
        let timeConversions = [86400, 3600, 60, 1];
        let timeUnits = ['d', 'h', 'm', 's'];
        for (let i = 0; i < timeConversions.length; i++) {
            if (timeInMs < timeConversions[i] && !time) continue;

            let mult = Math.floor(timeInMs / timeConversions[i]);
            time += mult + timeUnits[i];
            if (i >= 0 && time !== '') time += ' ';
            timeInMs -= timeConversions[i] * mult;
        }

        return time;
    }
}

module.exports = InfoCommand;
