const Command = require('../classes/Command.js');

/**
 * A command for changing your online status
 */
class StatusCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Change your online status',
            usage: [
                'status <status>'
            ],
            guildOnly: true
        });
        this.loadResources();
    }
    async run(message) {
        let args = message.content.split(' ').splice(1);
        let valid = this.validate(message, args);

        if (!valid) {
            this.logger.log('error', `Could not successfully run ${this.constructor.name}`);
        } else {
            this.bot.user.setStatus(args[0].toLowerCase());
        }

        message.delete();
    }

    loadResources() {
        this.statuses = require('../resources/statuses.json');
    }

    validate(message, args) {
        if (!message.guild && this.info.guildOnly) return false;
        if (!args || args.length < 1 || !this.statuses.includes(args[0])) return false;

        return true;
    }
}

module.exports = StatusCommand;
