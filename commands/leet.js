const Command = require('../classes/Command.js');

/**
 * A command for converting messages to l33tsp34k
 * @extends Command
 */
class LeetCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Converts a message into leetspeak',
            usage: [
                'leet <message>'
            ],
            guildOnly: false
        });
        this.loadResources();
    }

    async run(message) {
        let args = message.content.toLowerCase().split(' ').slice(1).join(' ').trim();
        let valid = this.validate(message, args);

        if (!valid) {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        } else {
            let leetText = this.getLeetText(args);

            message.edit(leetText);
        }
    }

    loadResources() {
        this.leetbrary = require('../resources/leetbrary.json');
    }

    validate(message, args) {
        if (!args) return false;

        return true;
    }

    /**
     * Converts text into l33tsp34k
     * @param {string} text - the text to convert
     * @return {string} the converted leetspeak
     */
    getLeetText(text) {
      return text.split('').map(letter => this.getLeetLetter(letter)).join('');
    }

    /**
     * Converts a letter into its l33tsp34k counterpart using the leetbrary
     * @param {string} letter - the letter to convert
     * @return {string} the converted letter
     */
    getLeetLetter(letter) {
        let leetLetter = '';

        if (letter in this.leetbrary) {
            leetLetter = this.leetbrary[letter][Math.floor(Math.random() * this.leetbrary[letter].length)] + ' ';
        } else if (letter === ' ') {
            leetLetter = letter.repeat(4);
        } else {
            leetLetter = letter;
        }

        return leetLetter;
    }
}

module.exports = LeetCommand;
