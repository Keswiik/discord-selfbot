const PlayMusic = require('playmusic');
const { RichEmbed } = require('discord.js');
const Command = require('../classes/Command.js');

/**
 * A command for posting information about songs from the
 * Google Play Music API
 */
class MusicInfoCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Use the GPM API to get song information',
            usage: [
                'musicinfo <search>'
            ]
        });
        this.loadResources();
    }

    async run(message) {
        let args = message.content.split(' ').slice(1).join(' ').trim();
        let valid = this.validate(message, args);

        if (valid) {
            this.bot.pm.search(args, 5, function(err, data) {
                if (err) {
                    this.logger.err(err.stack);
                    message.delete();
                    return;
                }

                let entries = data.entries.filter(e => e.type === '1');
                if (entries.length === 0) {
                    this.logger.err(`No results found for query: ${args}`);
                    message.delete();
                    return;
                }

                let song = entries[0];
                let duration = song.track.durationMillis;
                let timeMinutes = Math.floor((Math.floor(duration / 1000)) / 60);
                let timeSeconds = Math.floor(Math.floor(duration / 1000) - (timeMinutes * 60));
                let embed = new RichEmbed()
                    .setAuthor(`${song.track.title} by ${song.track.artist}`)
                    .setTitle(`Store ID: ${song.track.storeId}`)
                    .setColor(0x00FFFF)
                    .addField("Album", song.track.album, true)
                    .addField("Track", song.track.trackNumber, true)
                    .addField("Genre", song.track.genre, true)
                    .addField("Length", `${timeMinutes}:${timeSeconds}`, true)
                    .addField("Size", `${(parseInt(song.track.estimatedSize) / 1000000).toFixed(3)}MB`, true);
                if (song.track.albumArtRef.length > 0) {
                    embed.setThumbnail(song.track.albumArtRef[0].url);
                }

                message.edit({ embed });
            });
        }
    }

    validate(message, args) {
        if (!args) return false;

        return true;
    }

    loadResources() {
        if (!this.bot.pm) {
            this.bot.pm = new PlayMusic();
            this.bot.pm.init({androidId: process.env.GPM_AID, masterToken: process.env.GPM_MTOKEN}, function (err) {
                if (err) this.logger.err(err.stack);
            });
        }
    }
}

module.exports = MusicInfoCommand;

// pm.getStreamUrl(song.track.storeId, function(arg, url) {
//     var file = fs.createWriteStream('file.mp3')
//     var req = http.get(url, function (res) {
//         if (res) res.pipe(file)
//     })
// })
