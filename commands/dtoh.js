const Command = require('../classes/Command.js');

/**
 * A command used to convert decimal numbers to hex
 * @extends Command
 */
class DecimalToHexCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'a decimal number to hex',
            usage: [
                'dtoh <decimal_number'
            ]
        });
        this.decimalRegex = /^[0-9]+$/gi;
    }

    async run(message) {
        let args = this.extractArgs(message.content.split(' ').slice(1).join(' ').trim());
        let valid = this.validate(message, args);

        if (!valid) {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        } else {
            let hex = this.getHex(args);
            message.edit(`\`${args}\` in hex is \`0x${hex}\``);
        }
    }

    validate(message, args) {
        if (!args) {
            return false;
        }

        return true;
    }

    extractArgs(content) {
        if (this.decimalRegex.test(content)) {
            return content;
        }

        return undefined;
    }

    getHex(numberString) {
        return parseInt(numberString).toString(16).toUpperCase();
    }
}

module.exports = DecimalToHexCommand;
