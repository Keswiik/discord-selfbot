const Command = require('../classes/Command.js');

/**
 * A command to post macro'd replies into chat.
 */
class TagCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Tags for quick macros in chat',
            usage: [
                '-<tag_name>',
                '-tag <tag_name'
            ]
        });
        this.directRegex = /^-[-a-zA-Z\s]+$/;
        this.indirectRegex = /^-tag\s*[-a-zA-Z\s]+$/;
        if (!this.bot.tags) {
            this.tags = require('../resources/tags.json');
        }
    }

    async run(message) {
        let args = this.extractArgs(message.content);
        let valid = this.validate(message, args);

        if (valid) {
            message.edit(this.bot.tags[args]);
        } else {
            this.logger.err(`Failed to run command with args ${args}`);
        }
    }

    extractArgs(content) {
        if (this.indirectRegex.test(content)) {
            return content.replace(/^-tag\s*/, '');
        } else if (this.directRegex.test(content)) {
            return content.replace('-', '');
        }

        return undefined;
    }

    validate(message, args) {
        if (!args || args.length === 0) {
            return false;
        }

        return args in this.bot.tags;
    }
}

module.exports = TagCommand;
