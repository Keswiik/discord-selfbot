const google = require('google');
google.resultsPerPage = 5;
const Command = require('../classes/Command.js');

/**
 * A command used for searching google for search results
 * @extends Command
 */
class GoogleCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Get the first link from a google search',
            usage: [
                'google <search>'
            ]
        });
    }

    async run(message) {
        let args = message.content.split(' ').slice(1).join(' ').trim();
        let valid = this.validate(message, args);

        if (valid) {
            google(args, (err, res) => {
                if (err) {
                    this.logger.err(err.stack);
                    message.delete();
                } else if (res.links.length >= 1){
                    this.logger.info(res);
                    message.edit(`${message.content}: ${res.links[0].href}`);
                } else {
                    message.edit(`${message.content}: \`no results found\``);
                }
            });
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        }
    }

    validate(message, args) {
        if (!args) return false;

        return true;
    }
}

module.exports = GoogleCommand;
