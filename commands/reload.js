const Command = require ('../classes/Command.js');

/**
 * A command used to reload other commands
 */
class ReloadCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Changes your online status',
            usage: [
                'reload <command_name>'
            ],
            guildOnly: false
        });
    }

    async run(message) {
        let args = message.content.split(/\s+/g).slice(1);
        let valid = this.validate(message, args);

        if (!valid) {
            this.logger.err(`Failed to run command with args: ${args}`);
        } else {
            delete require.cache[require.resolve(`./${args[0]}.js`)];
            let Command = require(`./${args[0]}.js`);
            this.bot.commands[args[0]] = new Command(this.bot);
            this.logger.info(`Reloaded command ${args[0]}`);
        }

        message.delete();
    }

    validate(message, args) {
        if (!args || args.length != 1 || !(args[0] in this.bot.commands)) return false;

        return true;
    }
}

module.exports = ReloadCommand;
