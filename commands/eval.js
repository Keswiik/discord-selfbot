const { RichEmbed } = require('discord.js');
const Command = require('../classes/Command.js');

/**
 * A command for evaluating JavaScript within the bot and
 * posting the results in discord
 * @extends Command
 */
class EvalCommand extends Command {
  constructor(bot) {
    super(bot, {
      desc: 'Run some simple JavaScript',
      usage: [
        'eval <code>'
      ],
      guildOnly: false
    });
  }

  async run(message) {
    let args = message.content.split(' ').slice(1).join(' ').trim();
    let valid = this.validate(message, args);

    if (valid) {
      let embed = new RichEmbed()
        .setColor(0x00FFFF)
        .addField('*Input*', `\`\`\`js\n${args}\`\`\``, false);

      try {
        let start = process.hrtime();
        let output = eval(args);
        let end = process.hrtime();
        let time = (((end[0] - start[0]) * 1000000000) +
          (end[1] - start[1])) / 1000000;

        if (!(output instanceof Promise)) {
          output = this.trimOutput(this.stringifyOutput(output));
        }

        embed.addField('*Output*', `\`\`\`js\n${this.clean(output)}\`\`\``, false)
          .setFooter(`Time elapsed: ${time.toFixed(3)}ms`);
      } catch (err) {
        embed.addField('*Output*', `\`${this.trimOutput(err.stack)}\``);
        this.logger.err(err.stack);
      }
      message.edit({ embed });
    } else {
      this.logger.err(`Failed to run command with args: ${args}`);
      message.delete();
    }
  }

  /**
   * Cleans text so it can be properly evaluated
   * @param {string} text - the code to be cleaned
   * @return {string} the cleaned text
   */
  clean(text) {
    if (typeof text === 'string') {
      return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g,
        "@" + String.fromCharCode(8203));
    } else {
      return text;
    }
  }

  validate(message, args) {
    if (!args) return false;

    return true;
  }

  /**
   * Converts evaluated output to a string
   * @param {*} data - the data to be stringified
   * @return {string} the converted data
   */
  stringifyOutput(data) {
    if (typeof data === Object.name.toLowerCase()) {
      return JSON.stringify(data, null, 4);
    }

    return data.toString();
  }

  /**
   * Trims output to a maximum of 600 characters
   * @param {string} data - the string output to trim
   * @return {string} the trimmed string
   */
  trimOutput(data) {
    if (data.length >= 594) {
      return data.substring(0, 597) + '...';
    }

    return data;
  }
}

module.exports = EvalCommand;
