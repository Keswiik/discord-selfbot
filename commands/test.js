const Command = require('../classes/Command.js');

class TestCommand extends Command {
    constructor(bot) {
        super(bot, {});
    }

    async run(message) {
        let args = message.content.trim().split(' ').slice(1).shift();
        message.channel.send(args).then(message.channel.send(undefined)).catch(message.channel.send('fail'));
    }
}

module.exports = TestCommand;
