const Pokedex = require('pokedex-promise-v2');
const { RichEmbed } = require('discord.js');
const Command = require('../classes/Command.js');
const utils = require('../utils.js');

/**
 * A command for posting information about Pokemon
 */
class PokemonCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Get information about a Pokemon by name or ID',
            usage: [
                'pokemon <name/id>'
            ]
        });
        this.pokedex = new Pokedex();
        this.loadResources();
    }

    async run(message) {
        let args = message.content.split(' ').splice(1).join(' ');
        let valid = this.validate(message, args);

        if (valid) {
            let pokeData = await this.getPokemonData(args);
            let statString = pokeData. stats.map((stat) =>
                `\`${this.statTrans[stat.stat.name]}: ${stat.base_stat}\``).join(' ');
            this.logger.info(statString);
            let typeString = pokeData.types.map((type) => `\`${type.type.name}\``).join('');

            let embed = new RichEmbed()
                .setTitle(`${pokeData.name} (#${pokeData.id})`)
                .setThumbnail(pokeData.sprites.front_default)
                .setColor(0x00FFFFFF)
                .addField('Height', pokeData.height, true)
                .addField('Weight', pokeData.weight, true)
                .addField('Base Exp', pokeData.base_experience, true)
                .addField('Stats', statString, false)
                .addField('Types', typeString, false);

            message.edit({ embed });
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        }
    }

    validate(message, args) {
        if (!args) return false;

        return true;
    }

    loadResources() {
        this.statTrans = require('../resources/pokemon-stat-abbreviations.json');
        this.stats = require('../resources/pokemon-stats-to-save.json');
        this.pokeInfo = require('../resources/pokemon.json');
    }

    /**
     * Tries to retrieve pokemon info from cache, otherwise pulls it from the api
     * @param {string} data - the name or ID of a pokemon
     * @return {Object} the pokemmon's data
     */
    async getPokemonData(data) {
        let usingId = /^[0-9]+$/g.test(data);
        let isCached = (usingId ? this.pokeInfo.nameMap[data] : this.pokeInfo[data]) !== undefined;

        if (isCached) {
            return (usingId) ? this.pokeInfo[this.pokeInfo.nameMap[data]] : this.pokeInfo[data];
        } else {
            let rawData = await this.pokedex.getPokemonByName(data);
            let pokeData = {};

            this.stats.forEach((stat) => {
                pokeData[stat] = rawData[stat];
            });
            this.pokeInfo[pokeData.name] = pokeData;
            this.pokeInfo.nameMap[pokeData.id] = pokeData.name;
            utils.saveJSON(this.pokeInfo, './pokemon.json');

            return pokeData;
        }
    }
}

module.exports = PokemonCommand;
