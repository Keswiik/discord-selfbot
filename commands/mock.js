const Command = require('../classes/Command.js');

/**
 * A command for MoCkInG things said by yourself or other people.
 */
class MockCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: "WhEn yOu wAnT To mOcK SoMeOnE",
            command: [
                'mock -m <message id>',
                'mock <text>'
            ]
        });
    }

    async run(message) {
        let args = await this.extractArgs(message.channel,
            message.content.split(' ').splice(1).join(' ').trim());
        let valid = this.validate(message, args);


        if (valid) {
            let mockText = this.mock(args);
            message.edit(mockText);
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        }
    }

    validate(message, args) {
        if (!message || !args || args.length === 0) return false;

        return true;
    }

    /**
    * Extracts and retrieves text to be mocked.
    *
    * @param {Channel} channel - the channel to retrieve the message to mock, if necessary
    * @param {string} content - the content of the message that invoked the command
    * @return {string|undefined} - the text to mock, or undefined if an error occurred
    */
    async extractArgs(channel, content) {
        let messageRegex = /^\s*[0-9]+\s*-m|\s*-m\s*[0-9]+\s*$/gi;
        let text = content;

        // check if using message ID
        if (messageRegex.test(content)) {
            let messageID = content.replace(/(-m|\s+)*/gi, '');
            text = await this.getMockMessageText(messageID, channel);
        }

        return text;
    }

    /**
    * Retrieves a message and returns its text
    *
    * @param {string} messageID - the {Snowflake} ID of the message to retrieve
    * @param {Channel} channel - the channel to retrieve the message from
    * @return {string|undefined} - the content of the message,
    *            or undefined if an error occurs
    */
    async getMockMessageText(messageID, channel) {
        try {
            let messageToMock = await channel.fetchMessage(messageID);
            return messageToMock.content;
        } catch (err) {
            return undefined;
        }
    }

    /**
    * Alternates upper and lower-case letters
    *
    * @param {string} args - the text to mock
    * @return {string} - the mocked text
    */
    mock(args) {
        return args.split('').map(function(c, idx) {
            return (idx % 2 === 0) ? c.toUpperCase() : c.toLowerCase();
        }).join('');
    }
}

module.exports = MockCommand;
