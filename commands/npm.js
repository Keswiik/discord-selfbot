const Command = require('../classes/Command.js');

/**
 * A command for posting links to the NPM JS website.
 * Pretty much just linking to packages.
 */
class NpmCommand extends Command {
  constructor(bot) {
    super(bot, {
      desc: 'Provide a link to an NPM.js package',
      usage: [
        'npm <package>'
      ],
      guildOnly: false
    });
  }

  async run(message) {
    let args = message.content.slice(1).split(' ').slice(1).join('').trim();
    let valid = this.validate(message, args);

    if (valid) {
      let link = `https://www.npmjs.com/package/${args}`;
      message.edit(link);
    } else {
      this.logger.err(`Failed to run command with args: ${args}`);
      message.delete();
    }
  }

  validate(message, args) {
    if (!args) return false;

    return true;
  }
}

module.exports = NpmCommand;
