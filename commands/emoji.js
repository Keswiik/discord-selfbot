const Command = require('../classes/Command.js');

/**
 * A command used to convert text into regional indicator blocks
 * @extends Command
 */
class EmojiCommand extends Command {
  constructor(bot) {
    super(bot, {
      desc: 'Put your message in regional indicator blocks',
      usage: [
        'emoji <message>'
      ],
      guildOnly: false
    });
    this.loadResources();
  }

  async run(message) {
    let args = message.content.split(' ').splice(1).join(' ').toLowerCase();
    let valid = this.validate(message, args);

    if (valid) {
      let emojiString = this.generateEmojiText(args);
      this.logger.info(emojiString);
      message.edit(emojiString);
    } else {
      this.logger.err(`Failed to run command with args: ${args}`);
      message.delete();
    }
  }

  /**
   * Generates the emoji-string given input text
   * @param {string} text - the text to convert to emoji
   * @return {string} the converted emoji
   */
  generateEmojiText(text) {
    let emojiText = '';
    text.split('').forEach(letter => {
      if (/[a-z]/g.test(letter)) {
          emojiText += `:regional_indicator_${letter}:`;
      } else if (/[0-9]/g.test(letter)) {
          emojiText += this.numberMap[letter];
      } else if (letter === '!') {
          emojiText += ':exclamation:';
      } else if (letter === '?') {
          emojiText += ':question:';
      } else if (letter === ' ') {
          emojiText += letter.repeat(2);
      } else {
          emojiText += letter;
      }
    });

    return emojiText;
  }

  loadResources() {
    this.numberMap = require('../resources/number-emoji-map.json');
  }

  validate(message, args) {
    if (!args) return false;

    return true;
  }
}

module.exports = EmojiCommand;
