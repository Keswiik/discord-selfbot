const Command = require('../classes/Command.js');
const { RichEmbed } = require('discord.js');
const moment = require('moment');

class QuoteCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Quote a message someone else sent',
            usage: [
                'quote <message_id>'
            ]
        });
    }

    async run(message) {
        let args = message.content.split(/\s+/g).slice(1).join('').trim();
        let valid = this.validate(message, args);

        if (valid) {
            let quoteMessage = await message.channel.fetchMessage(args);
            let embed = new RichEmbed()
                .setAuthor(quoteMessage.member.displayName, quoteMessage.author.displayAvatarURL)
                .setDescription(quoteMessage.content)
                .setFooter(`in #${quoteMessage.channel.name} | ${moment(quoteMessage.createdAt).format('LLLL')}`)
                .setColor(0x00FFFF);
            message.edit({ embed });
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        }
    }

    validate(message, args) {
        if (!args) {
            return false;
        }

        return /[0-9]+/.test(args);
    }
}

module.exports = QuoteCommand;
