const fs = require('fs');
const sprintf = require('sprintf-js').sprintf;
const Command = require('../classes/Command.js');

/**
 * A command to show all saved memes
 */
class MemesCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Shows the meme list',
            usage: []
        });
        this.loadResources();
    }

    async run(message) {
        let list = this.getFormattedMemeList();
        let currentMessage = '';
        let line = 0;
        while (line < list.length) {
            if (currentMessage.length + list[line].length >= 1994) {
                message.channel.send(currentMessage, {code: 'xl'});
                currentMessage = '';
            }

            currentMessage += list[line];
            line++;
        }

        if (currentMessage) {
            message.channel.send(currentMessage, {code: 'xl'});
        }
    }

    loadResources() {
        if (!this.bot.memes) {
            this.bot.memes = require('../resources/memes.json');
        }
    }

    /**
     * Returns a list of all saved memes with a specified type
     * @param {string} type - the type of memes to find
     * @return {Array} an array of matching meme names
     */
    getMemesWithType(type) {
        let matchedMemes = [];
        for (let key in this.bot.memes) {
            if (this.bot.memes[key].type === type) {
                matchedMemes.push(key);
            }
        }

        return matchedMemes;
    }

    /**
     * Formats the meme list line-by-line in an array
     * array is used to facilitate formatting/splitting of list into multiple
     * messages
     * @return {Array} an array consisting of individual lines of the message
     */
    getFormattedMemeList() {
        let types = ["image", "text"];
        let formattedMessage = ['\nMemes\n'];
        for (let type of types) {
            formattedMessage.push('\t' + type + '\n');
            let matchedMemes = this.getMemesWithType(type);
            matchedMemes = matchedMemes.sort();
            for (let meme of matchedMemes) {
                if (meme in this.bot.memes) {
                    let numFiles = '';
                    if (type === 'image') {
                        numFiles = `(${fs.readdirSync('./memes/' + meme).length})`;
                    }
                    let line = '\t\t- ' + sprintf('%-13s %5s: ', meme, numFiles);
                    meme = this.bot.memes[meme];
                    if ('desc' in meme) {
                        line += meme.desc;
                    }
                    formattedMessage.push(line + '\n');
                }
            }
        }
        return formattedMessage;
    }
}

module.exports = MemesCommand;
