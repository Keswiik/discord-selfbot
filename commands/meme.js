const fs = require('fs');
const sprintf = require('sprintf-js').sprintf;
const Command = require('../classes/Command.js');

/**
 * A command used for posting memes
 * @extends Command
 */
class MemeCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Posts a shitty meme',
            usage: [
                'meme <meme_name>'
            ]
        });
        this.loadResources();
    }

    async run(message) {
        let args = message.content.split(' ').slice(1);
        let valid = this.validate(message, args);

        if (valid) {
            let memeName = args[0];
            if (memeName in this.bot.memes) {
                let memeData = this.bot.memes[memeName];
                let meme = this.getMeme(memeData, memeName);
                if (meme) {
                    this.sendMeme(message, meme);
                } else {
                    this.logger.err(`Failed to fetch meme for: ${meme.name}`);
                }
            } else {
                this.logger.err(`Attempted to send unknown meme: ${args[0]}`);
            }
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
        }
    }

    /**
     * Fetches a meme's response
     * @param {Object} memeData - Basic information about a meme
     * @param {string} memeName - The name of the meme
     * @return {Object} modified memeData containing the response
     */
    getMeme(memeData, memeName) {
        if (memeData.type === 'image') {
            return this.getImageMeme(memeName);
        } else if (memeData.type === 'text') {
            return this.getTextMeme(memeName);
        }

        return undefined;
    }

    /**
     * Sends a meme, either by uploading a picture or sending text
     * If sending an image, the original message will be deleted
     * @param {Message} message - the message that invoked this command
     * @param {Object} meme - the meme to send
     */
    async sendMeme(message, meme) {
        if (meme.type === 'image') {
            try {
                await message.channel.send({
                    files: [{
                        attachment: meme.meme,
                        name: meme.filename
                    }]
                });
                message.delete();
            } catch (err) {
                this.logger.err(err.stack);
                message.delete();
            }
        } else if (meme.type === 'text'){
            message.edit(meme.meme);
        }
    }

    validate(message, args) {
        if (!args || args.length < 1) return false;

        return true;
    }

    loadResources() {
        if (!this.bot.memes) {
            this.bot.memes = require('../resources/memes.json');
        }
    }

    /**
     * Fetches an image meme
     * @param {Object} meme - the meme to fetch
     * @return {Object} the fetched meme, including image data
     */
    getImageMeme(meme) {
        let files = fs.readdirSync(`${process.env.MEMEDIR}/${meme}`);
        let file = `./memes/${meme}/${files[(Math.floor(Math.random() * files.length))]}`;
        let image = fs.readFileSync(file);

        return {
            meme: image,
            filename: file,
            type: 'image'
        };
    }

    /**
     * Fetches a text meme
     * @param {Object} meme - Fetches a text meme
     * @return {Object} the fetched meme including a text response
     */
    getTextMeme(meme) {
        let reply;
        if (this.bot.memes[meme].usesMethod) {
            reply = eval(this.bot.memes[meme].method);
        } else {
            reply = this.bot.memes[meme].reply;
        }
        return {
            meme: reply,
            type: 'text'
        };
    }

    squareE(text) {
        let sideLength = 1;
        let spaces = 1;
        while (spaces < text.length) {
            spaces = (sideLength * 2) + ((sideLength - 2) * 2);
            if (spaces < text.length) {
                sideLength += 1;
            }
        }

        let textArr = [];
        for (let i = 0; i < sideLength; i++) {
            textArr.push([]);
            for (let j = 0; j < sideLength; j++) {
                textArr[i].push(" ");
            }
        }

        let space = 0;
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < sideLength - 1; j++) {
                if (i === 0) {
                    textArr[0][j] = text.charAt(space);
                } else if (i === 1) {
                    textArr[j][sideLength - 1] = text.charAt(space);
                } else if (i === 2) {
                    textArr[sideLength - 1][sideLength - 1 - j] = text.charAt(space);
                } else if (i === 3) {
                    textArr[sideLength - 1 - j][0] = text.charAt(space);
                }

                space ++;
                if (space === text.length) {
                    break;
                }
            }
        }

        let newText = '```\n';
        for (let i = 0; i < textArr.length; i++) {
            for (let j = 0; j < textArr.length; j++) {
                newText += textArr[i][j] + ' ';
            }
            newText += '\n';
        }
        newText += '```';

        return newText;
    }

    squareM(text) {
        let length = text.length;
        let textArr = [];
        for (let i = 0; i < length; i++) {
            textArr.push([]);
            for (let j = 0; j < length * 2 - 1; j++) {
                textArr[i].push(' ');
            }
        }

        for (let i = 0; i < length; i++) {
            if (i === 0) {
                for (let j = 0; j < length; j++) {
                    textArr[i][j * 2] = text.charAt(j);
                }
            } else if (i === length - 1) {
                for (let j = 0; j < length; j++) {
                    textArr[i][j * 2] = text.charAt(length - j - 1);
                }
            } else {
                textArr[i][0] = text[i];
                textArr[i][length * 2 - 2] = text.charAt(length - i - 1);
            }
        }

        let newText = '```\n';
        for (let i = 0; i < textArr.length; i++) {
            for (let j = 0; j < textArr[i].length; j++) {
                newText += textArr[i][j];
            }
            newText += '\n';
        }
        newText += '```';

        return newText;
    }

    gayMeter(message) {
        let user = (message.mentions.users.size > 0) ? message.mentions.users.first() : message.author;
        let length = 40;
        let gayness = Math.floor(Math.random() * 40);
        let padding = Array(Math.floor((user.length / 2)) + 1).join(' ');
        if (padding.length < 2) {
            padding = '  ';
        }

        let meter = 'Here is how gay ' + user + ' is\n\n\`\`\`';
        meter += Array((gayness + 1)).join(' ') + user.username + '\n';
        meter += padding + ' '.repeat(gayness) + '|\n';
        meter += padding + '_'.repeat(length) + '\n';
        meter += padding + sprintf('%-10s%-10s%-10s%-9s|\n', '|', '|', '|', '|');
        meter += padding + Array(length + 1).join('-') + '\n';
        meter += padding.slice(2) + 'ungay     kinda    moderate    very   rainbow\n';
        meter += '\`\`\`';

        return meter;
    }
}

module.exports = MemeCommand;
