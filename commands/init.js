const Command = require('../classes/Command.js');

/**
 * A command for initializing new commands
 * @extends Command
 */
class InitCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Initializes a new command',
            usage: [
                'init <command>'
            ]
        });
    }

    async run(message) {
        let args = message.content.split(' ').shift().join(' ').trim();
        let valid = this.validate(message, args);

        if (valid) {
            let Command = require(`./${args}.js`);
            this.bot.commands[args] = new Command(this.bot);
            this.logger.info(`Successfully initialized command ${args}`);
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
        }

        message.delete();
    }

    validate(message, args) {
        if (!args || !(args in this.bot.commands)) return false;

        return true;
    }
}

module.exports = InitCommand;
