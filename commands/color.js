const Command = require('../classes/Command.js');
const { RichEmbed } = require('discord.js');

/**
 * A command used to display colors within an embed
 * @extends Command
 */
class ColorCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Shows some colors within an embed',
            usage: [
                'color <color_code>'
            ],
            guildOnly: false
        });
        this.hexRegex = /^(?:0x|#)[0-9a-f]+$/gi;
        this.decimalRegex = /^[0-9]+$/g;
    }

    async run(message) {
        let args = this.extractArgs(message.content.split(' ').slice(1).join(' ').trim());
        let valid = this.validate(message, args);

        if (!valid) {
            this.logger.err(`Failed to run with args: ${args}`);
            message.delete();
        } else {
          let color = parseInt(args);
          let embed = new RichEmbed()
              .setColor(color)
              .setDescription(`<--[\`${color}\` | \`0x${color.toString(16).toUpperCase()}\`]`);

          message.edit({ embed });
        }
    }

    extractArgs(data) {
        if (this.decimalRegex.test(data)) {
            return data;
        } else if (this.hexRegex.test(data)) {
            return data.replace(/#/gi, '0x');
        }

        return undefined;
    }

    validate(message, args) {
        if (!message.guild && this.info.guildOnly) return false;
        if (!args) return false;

        return true;
    }
}

module.exports = ColorCommand;
