const Command = require('../classes/Command.js');

/**
 * A command for converting hex numbers to decimal
 * @extends Command
 */
class HexToDecimalCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Convert a hex number to decimal',
            usage: [
                'htod <hex_number>'
            ],
            guildOnly: false
        });
        this.hexRegex = /^(?:0x|#)[0-9a-f]+$/gi;
    }

    async run(message) {
        let args = this.extractArgs(message.content.split(' ').slice(1).join(' ').trim());
        let valid = this.validate(message, args);

        if (valid) {
            let decimal = parseInt(args);
            message.edit(`\`${args}\` in decimal is \`${decimal}\``);
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
            message.delete();
        }
    }

    validate(message, args) {
        if (!this.hexRegex.test(args)) return false;

        return true;
    }

    extractArgs(data) {
        return data.replace(/0x|#/gi, '0x');
    }
}

module.exports = HexToDecimalCommand;
