const Command = require('../classes/Command.js');
const utils = require('../utils.js');

/**
 * A command for adding tags to the bot.
 */
class AddTagCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Adds tags to the bot',
            usage: [
                'addTag <tag_name> <tag_content>'
            ]
        });

        if (!this.bot.tags) {
            this.bot.tags = require('../resources/tags.json');
        }
    }

    async run(message) {
        let data = message.content.split(' ').slice(1);
        let args = [data.shift(), data.join(' ')];
        let valid = this.validate(message, args);

        if (valid) {
            this.bot.tags[args[0]] = args[1];
            utils.saveJSON(this.bot.tags, './resources/tags.json');
        } else {
            this.logger.err(`Failed to run command with args: ${args}`);
        }

        message.delete();
    }

    validate(message, args) {
        if (!args[0].length && !args[1].length) return false;

        return true;
    }
}

module.exports = AddTagCommand;
