const utils = require('../utils.js');
const request = require('request');
const fs = require('fs');
const Command = require('../classes/Command.js');

/**
 * A command used to add memes to memes.json
 * @extends Command
 */
class AddMemeCommand extends Command {
    constructor(bot) {
        super(bot, {
            desc: 'Download images or save text memes',
            usage: [
                'addmeme <image/text> <link/text>'
            ]
        });
        this.loadResources();
    }

    async run(message) {
        let args = message.content.split(' ').slice(1);
        let valid = this.validate(message, args);

        if (valid) {
            let meme = args.shift();
            let type = args.shift();
            let data = args.join(' ').trim();

            if (!this.bot.memes[meme]) {
                this.addEmptyMeme(meme, type);
                if (type === 'image') this.createMemeFolder(meme);
            }

            if (type === 'image') {
                this.downloadImage(data, meme);
            } else if (type === 'text') {
                this.bot.memes[meme].reply = data;
                this.bot.memes[meme].usesMethod = false;
            }

            utils.saveJSON(this.bot.memes, '../resources/memes.json');
        } else {
            this.logger.err(`Failed to run command with args: ${message.content}`);
        }

        message.delete();
    }

    validate(message, args) {
        if (!args || args.length < 3) return false;

        return true;
    }

    loadResources() {
        if (!this.bot.memes) {
            this.bot.memes = require('../resources/memes.json', 'utf8');
        }
    }

    /**
     * Downloads an image and saves it to the corresponding meme folder
     * @param {string} uri - the URI of the image to download
     * @param {string} meme - the name of the meme this image belongs to
     */
    downloadImage(uri, meme) {
        request.head(uri, (err, res) => {
            if (err) {
                this.logger.err(err.stack);
                return;
            }
            this.logger.info(`Downloading image from: ${uri}`);

            let name = Math.random().toString(36).replace(/[^a-zA-Z0-9\-]+/g, '');
            let filename = `${process.env.MEMEDIR}/${meme}/${name}.${res.headers['content-type'].split('/')[1]}`;
            request(uri).pipe(fs.createWriteStream(filename)).on('close', () => {
                this.logger.info(`File ${filename} successfully downloaded`);
            });
        });
    }

    /**
     * Initializes an empty meme within the bot's meme collection
     * @param {string} meme - the name of the meme to initialize
     * @param {string} type - the meme's type
     */
    addEmptyMeme(meme, type) {
        this.bot.memes[meme] = {
            type: type,
            data: ''
        };
    }

    /**
     * Creates a new folder for memes to be saved in
     * @param {string} meme - The name of the meme
     */
    createMemeFolder(meme) {
        if (!fs.existsSync(`${process.env.MEMEDIR}/${meme}`)) {
            fs.mkdirSync(`${process.env.MEMEDIR}/${meme}`);
        }
    }
}

module.exports = AddMemeCommand;
