const fs = require('fs');
// var logger = require('./Logger.js')

/**
* Replaces all instances of a string within another string
* @param {String} str - The string to replace values in
* @param {String} find - The string to search for and replace
* @param {String} replace - The string to replace 'find' with
*/
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

/**
* Saves a JSON file.
* @param {Object} json - The JSON object to save
* @param {String} file_dir - The location of the file to save data to
*/
function saveJSON(json, file_dir) {
  fs.writeFile(file_dir, JSON.stringify(json, null, 4), "utf8", function() {
    // logger.log("info", `File ${file_dir} was updated`)
});
}

/**
* Get the number of minutes between two dates
* Can probably be moved to neatbot.js
* @param {Date} date1 - The first date
* @param {Date} date2 - The second date
*/
function minuteDiff(date1, date2) {
    return Math.abs(date1 - date2) / (1000 * 60);
}

module.exports = {
    replaceAll: replaceAll,
    saveJSON: saveJSON,
    minuteDiff: minuteDiff
};
