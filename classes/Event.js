const Logger = require('./Logger.js');
const loggerConfig = require('../resources/loggerConfig.json');

/**
 * Generalized class to be used for implementing event listeners
 */
class Event {
    constructor(bot, name) {
        if (!name) {
            throw new Error('Cannot create event without a name');
        }
        //only relevant in constructor, so it's a closure
        let initialize = (propName, value) => {
            Object.defineProperty(this, propName, {
                enumerable: false,
                configurable: false,
                writable: false,
                value: value
            });
        };

        initialize('name', name);
        initialize('bot', bot);
        initialize('logger', new Logger(loggerConfig, this.constructor.name));
    }
}

module.exports = Event;
