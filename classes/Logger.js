const fs = require('fs');
const os = require('os');
const TextColorer = require('./TextColorer.js');
const moment = require('moment');

/**
* Object that caches WriteStreams for output files
* Used to prevent maintaining duplicate files handles between loggers
*/
const ioMap = {};

/**
* Need to open file in append mode
*/
const fileOptions = {
    flags: 'a'
};

/**
* Config to use as a base when copying user config values
*/
const defaultConfig = {
    outputDir: './log.txt',
    indent: true,
    tabSize: 4,
    levelMap: {
        info: 'green',
        error: 'red',
        warning: 'yellow'
    }
};

/**
 * A synchronous, (somewhat) configurable logger.
 */
class Logger {
    /**
     * Creates a new Logger instance
     *
     * @param {Object} config - object holding configuration values
     * @param {String} className - the (optional) class name the logger is for
     */
    constructor(config, className = '') {
        this.config = defaultConfig;
        this.className = className;
        if (config) {
            this.config = this.updateConfig(defaultConfig, config);
        }
        this.colorer = new TextColorer();

        if (!fs.existsSync(this.config.outputDir)) {
            this.logger.warn(`File ${this.config.outputDir} not found, attempting to create it`);
            fs.closeSync(fs.openSync(this.config.outputDir, 'w'));
        }

        if (!(this.config.outputDir in ioMap)) {
            ioMap[this.config.outputDir] = fs.createWriteStream(this.config.outputDir, fileOptions);
        }
    }

    /**
    * Used to copy values from a new config and replace those in an older config.
    * Just here to update the default config with user defined values.
    *
    * @param {Object} oldConfig - the config to use as a base
    * @param {Object} newConfig - the config to copy values from
    * @return {Object} the updated config
    */
    updateConfig(oldConfig, newConfig) {
        let clone = JSON.parse(JSON.stringify(oldConfig));
        Object.keys(newConfig).forEach((prop) => {
            if (prop in clone) {
                clone[prop] = newConfig[prop];
            }
        });
        return clone;
    }

    /**
     * Gets a formatted date string
     * @param {Date} date - the (optional) date to format.
     *              Date.now() will be used if not provided.
     * @return {String} the formatted date string
     */
    formatDate(date = Date.now()) {
        return moment(date).format('DD.MM.YYYY-hh:mm:ss');
    }

    /**
     * Gets a string representation for different objects.
     *
     * @param {*} data - the data to stringify
     * @return {String|undefined} String if valid data was provided, otherwise undefined
     */
    stringifyData(data) {
        if (!data) return undefined;

        if (typeof data === Object.name.toLowerCase()) {
            return JSON.stringify(data, null, 4);
        } else if (typeof data !== 'string') {
            return data.toString();
        }

        return data;
    }

    /**
     * Formats the level to be used by the console
     * Uses a TextColorer to color the level text
     *
     * @param {String} level - the level to format
     * @return {String} the formatted level
     */
    formatLevel(level) {
        return `[${this.colorer.color(level.toUpperCase(), this.config.levelMap[level])}]`;
    }

    /**
     * Formats the data to log
     *
     * @param {String} formattedLevel - the logging level after formatting (in '[]' brackets)
     * @param {String} dateString - the string after formatting through moment
     * @param {*} data - the data to format
     * @return {String} the formatted string
     */
    formatLogString(formattedLevel, dateString, data) {
        if (!data) {
            return undefined;
        } else {
            data = this.stringifyData(data);
        }

        //no need to format if no indentation is required
        if (!this.config.indent) {
            return data;
        }

        let arr = data.split('\n');
        let logString = (this.className ? `${this.className}: `: '') + arr.shift();
        let padding = ' '.repeat(this.config.tabSize + formattedLevel.length + dateString.length);
        arr.forEach((line, idx) => {
            logString += padding + line + (idx === arr.length - 1) ? '' : os.EOL;
        });

        return logString;
    }

    /**
     * Convenience function for warnings.

     * @param {*} data - the data to log
     */
    warn(...data) {
        this.log('warning', ...data);
    }

    /**
     * Convenience function for errors.

     * @param {*} data - the data to log
     */
    err(...data) {
        this.log('error', ...data);
    }

    /**
     * Convenience function for general log messages

     * @param {*} data - the data to log
     */
    info(...data) {
        this.log('info', ...data);
    }

    /**
     * Logs data to both the console and a file.
     * Uses level for coloring text. Custom levels can be configured.
     *
     * @param {String} level - the level of the log message (used for coloring text)
     * @param {*} data - the data to log
     */
    log(level, ...data) {
        if (!data || data.length === 0) return;

        let formattedLevel = `[${level.toUpperCase()}]`;
        let formattedColorLevel = this.formatLevel(level);
        data.forEach(datum => {
            let dateString = this.formatDate();
            let formattedData = this.formatLogString(formattedLevel, dateString, datum);

                console.log(`${dateString} ${formattedColorLevel} ${formattedData}`);
                if (ioMap[this.config.outputDir]) {
                    this.writeLog(`${dateString} ${formattedLevel} ${formattedData + os.EOL}`);
                }
            });
        }

        writeLog(data) {
            ioMap[this.config.outputDir].write(data);
        }
    }

    module.exports = Logger;
