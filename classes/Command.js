const Logger = require('./Logger.js');
const loggerConfig = require('../resources/loggerConfig.json');

class Command {
    constructor(bot, info = {}) {
        let initialize = (propName, value) => {
            Object.defineProperty(this, propName, {
                enumerable: false,
                configurable: false,
                writable: false,
                value: value
            });
        };

        initialize('bot', bot);
        initialize('logger', new Logger(loggerConfig, this.constructor.name));
        initialize('info', info);
    }
    async run() {
        throw new Error(`${this.constructor.name} has not implemented run()`);
    }
    extractArgs(content) {
      return content.split(/\s+/).slice(1);
    }
}

module.exports = Command;
