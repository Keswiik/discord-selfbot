const { Client } = require('discord.js');
const Logger = require('./Logger.js');
const loggerConfig = require('../resources/loggerConfig.json');
const fs = require('fs');
const dns = require('dns');

class SelfbotClient extends Client {
    constructor(options = {}) {
        super(options);
        this.logger = new Logger(loggerConfig, this.constructor.name);
        this.connected = true;
        this.loadCommands();
        this.loadEvents();
        this.setInterval(() => this.checkConnection(), 60000);
    }

    loadCommands() {
        this.commands = {};
        let commands = fs.readdirSync('./commands/');
        commands.forEach(commandName => {
            let Command = require(`../commands/${commandName}`);
            this.commands[commandName.replace('.js', '')] = new Command(this);
        });
    }

    loadEvents() {
        this.events = {};
        let events = fs.readdirSync('./events/');
        events.forEach(eventName => {
            let Event = require(`../events/${eventName}`);
            let event = new Event(this);
            this.events[event.name] = event;
        });
    }

    checkConnection() {
        dns.lookup('google.com', err => {
            if (err && err.code === 'ENOTFOUND') {
                this.logger.info('Awaiting active network connection');
            } else if (!this.connected){
                this.login(process.env.token);
                this.logger.info('Active connection detected, attempting to login...');
            }
        });
    }
}

module.exports = SelfbotClient;
