const Command = require('../classes/Command.js');
const utils = require('../utils.js');

class ReplacerCommand extends Command {
    constructor(bot) {
        super({
            desc: 'replaces keywords in chat',
            usage: [
                'replace <key> <content>'
            ]
        });

        this.keywords = require('../resources/replace.json');
    }

    async run(message) {
        let args = message.content.split(' ').slice(1);
        let valid = this.validate(args);

        if (valid) {
            let name = args.shift();
            let content = args.join(' ');
            this.keywords[name] = content;
            utils.saveJSON(this.keywords, './resources/replace.json');
        } else {
            this.logger.info(`Failed to run command with args ${args}`);
        }

        message.delete();
    }

    async replace(message) {
        let content = message.content;
        Object.keys(this.keywords).forEach(word => {
            content = content.replace(`\$${word}`, this.keywords[word]);
        });

        if (content !== message.content) {
            this.logger.info('Detected replacements in message, editing');
            message.edit(content);
        }
    }

    validate(message, args) {
        if (!args || args.length < 2) {
            return false;
        }

        return true;
    }
}

module.exports = ReplacerCommand;
