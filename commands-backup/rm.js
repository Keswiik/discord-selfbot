exports.desc = 'Removes a command from the bot'
exports.run = rm
exports.command = [
    'rm <command_name>'
]

var utils = require('../utils.js')
var logger = require('../Logger.js')

async function rm(bot, message) {
    var args = message.content.split(' ')
    if (args.length > 1 && args[1] in bot.commands) {
        delete require.cache[require.resolve(`./${args[1]}.js`)]
        delete bot.commands[args[1]]
        logger.log('info', `Successfully removed command ${args[1]}`)
    }

    message.delete()
}
