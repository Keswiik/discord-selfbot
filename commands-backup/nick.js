exports.run = nick
exports.desc = "Changes your nickname"
exports.command = [
    'nick <nickname>'
]

var logger = require('../Logger.js')

async function nick(bot, message) {
    if (message.channel.type === 'dm' || message.channel.type === 'group') return

    var name = message.content.split(' ').splice(1).join(' ').trim()
    if (name) {
        var updatedUser = await message.guild.member(message.author).setNickname(name)
        logger.log('info',
            `Nickname for guild ${message.guild.name} (${message.guild.id}) changed to ${updatedUser.nickname}`)
    }

    await message.delete()
}
