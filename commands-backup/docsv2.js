exports.run = docs
exports.desc = 'Real information about the docs'
exports.command = [
    'docs <class/typedef/interface> <member/method>'
]

var utils = require('../utils.js')
var logger = require('../Logger.js')

async function docs(bot, message) {
    var params = message.content.slice(1).split(' ').slice(1)
    var type
    var docsData
    if (params[0] in stable.classes) {
        type = 'class'
        docsData = stable.classes[params[0]]
    } else if (params[0] in stable.interfaces) {
        type = 'interface'
        docsData = stable.interfaces[params[0]]
    } else if (params[0] in stable.typedefs) {
        type = 'typedef'
        docsData = stable.typedefs[params[0]]
    }

    var link = `https://discord.js.org/#/docs/main/stable/${type}/${params[0]}`
    if (params.length === 2) {
        link += `?scrollTo=${params[1]}`
    }

    var embed = new Discord.RichEmbed()
    if (params.length == 2) {
        embed.setTitle(`${params[0]}#${params[1]}`)
    } else {
        if (docsData.extends) {
            embed.setTitle(`${params[1]} (extends ${docsData.extends})`)
        } else {
            embed.setTitle(params[1])
        }
    }

    embed.setURL(link)


}
