exports.run = help
exports.desc = "List the selfbot's commands."
exports.command = [
    "meme <meme_name>"
]

var utils = require ('../utils.js')
var logger = require('../Logger.js')
var Discord = require('discord.js')

async function help(bot, message) {
    var commands = utils.getCommands()
    var embed = new Discord.RichEmbed()
        .setAuthor('Commands', bot.user.displayAvatarURL)
    var keys = Object.keys(commands).sort()
    keys.forEach(function(key) {
        command = commands[key]
        var fieldData = command.desc
        command.command.forEach(function(usage) {
            fieldData += `\n\t\`${usage}\``
        })
        embed.addField(key, fieldData)
    })

    message.edit({embed})
}
