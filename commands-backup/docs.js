exports.run = docs
exports.desc = "Provide a link to the discord.js docs"
exports.command = [
    "-docs <Class>",
    "-docs <Class> <Member/Method>"
]

var utils = require ('../utils.js')

async function docs(bot, message) {
    var params = message.content.slice(1).split(' ').slice(1)
    var link = `https://discord.js.org/#/docs/main/stable/class/${params[0]}`
    if (params.length === 2) {
        link += `?scrollTo=${params[1]}`
    }

    await message.edit(link)
}
