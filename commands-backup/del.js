exports.run = del
exports.desc = "Delete your own messages"
exports.command = [
    "del <number_of_messages>"
]

var utils = require('../utils.js')
var logger = require ('../Logger.js')

async function del(bot, message) {
    var numMessages = parseInt(message.content.split(' ')[1]) + 1
    logger.log('info', `Deleting ${numMessages} messages.`)

    var messages = await message.channel.fetchMessages({limit: 100})
    var messages = messages.array().filter(m => m.author.id === bot.user.id).sort(function(a, b) {
        a.createdAt - b.createdAt
    })
    logger.log('info', `Found ${messages.length} messages`)
    var deleted = 0
    messages.some(function (foundMessage) {
        foundMessage.delete()
        .then(function(deletedMessage) {
            logger.log('info', `Deleted message ${deletedMessage.content}`)
        })
        deleted++

        return deleted === numMessages
    })
}
