const Event = require('../classes/Event.js');
const Discord = require('discord.js');

/**
 * Implementation of the listener called when the bot is ready and online
 */
class ReadyEvent extends Event {
    constructor(bot) {
        super(bot, 'ready');
        this.caller = (...args) => this.run(...args);
        this.bot.on(this.name, this.caller);
    }

    async run() {
        this.logger.info(`Kesbot v0.1 online using Discord.js v${Discord.version}` +
            ` and Node.js ${process.version}`);
        this.bot.connected = true;
    }

    unbind() {
        this.bot.removeListener(this.name, this.caller);
    }
}

module.exports = ReadyEvent;
