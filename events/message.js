const Event = require('../classes/Event.js');

/**
 * Implementation of the listener called when the client receives a message
 */
class MessageEvent extends Event {
    constructor(bot) {
        super(bot, 'message');
        this.caller = (...args) => this.run(...args);
        this.bot.on(this.name, this.caller);
    }

    async run(message) {
        //selfbots only respond to the account they're logged in under
        if (message.author.id !== this.bot.user.id) {
            return;
        }
        // message must start with command prefix
        if (message.content.startsWith('-')) {
            let command = message.content.slice(1).split(' ')[0];
            if (command in this.bot.commands) {
                try {
                    this.logger.info(`Invoking command ${command}`);
                    await this.bot.commands[command].run(message);
                } catch (err) {
                    this.logger.err(`Unexpected exception occurred while running command ${command}: \n${err.stack}`);
                }
            } else {
                this.bot.commands.tag.run(message);
            }
        } else {
            // this.bot.commands.replacer.replace(message);
        }
    }

    unbind() {
        this.bot.removeListener(this.name, this.caller);
    }
}

module.exports = MessageEvent;
