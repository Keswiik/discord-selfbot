const Event = require('../classes/Event.js');

class ErrorEvent extends Event {
    constructor(bot) {
        super(bot, 'error');
        this.caller = (...args) => this.run(...args);
        this.bot.on(this.name, this.caller);
    }

    async run(error) {
        this.logger.err(`Unhandled error event:\n${error.message}\n${error.stack}`);
    }

    unbind() {
        this.bot.removeListener(this.bot, this.caller);
    }
}

module.exports = ErrorEvent;
