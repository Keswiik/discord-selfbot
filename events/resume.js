const Event = require('../classes/Event.js');

class ResumeEvent extends Event {
    constructor(bot) {
        super(bot, 'resume');
        this.bot.on(this.name, (...args) => this.run(...args));
    }

    async run(replayed) {
        this.logger.info(`Connection resumed, replayed ${replayed} events`);
        this.bot.connected = true;
    }

    unbind() {
        this.bot.removeListener(this.name, this.caller);
    }
}

module.exports = ResumeEvent;
