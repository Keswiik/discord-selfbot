const Event = require('../classes/Event.js');

class DisconnectEvent extends Event {
    constructor(bot) {
        super(bot, 'disconnect');
        this.bot.on(this.name, (...args) => this.run(...args));
    }

    async run(closeEvent) {
        this.logger.err(closeEvent);
        this.bot.connected = false;
    }

    unbind() {
        this.bot.removeListener(this.name, this.caller);
    }
}

module.exports = DisconnectEvent;
