const Event = require('../classes/Event.js');

class ReconnectingEvent extends Event {
    constructor(bot) {
        super(bot, 'reconnecting');
        this.bot.on(this.name, (...args) => this.run(...args));
    }

    async run() {
        this.logger.err('Attempting to reconnect');
    }

    unbind() {
        this.bot.removeListener(this.name, this.caller);
    }
}

module.exports = ReconnectingEvent;
